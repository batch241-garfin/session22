
// Non-Mutator Methods //

// Non-mutator methods are functions that do not modify or change an
// array after they have been created
// These methods do not manipulate the original array
// These methods perform tasks such as returning or getting elements
// from an array, combining arrays, and printing the output
// indexOf(), lastIndexOf(), slice(), toStrng(), concat(), join()

let countries = ["US","PH","CAN","SG","TH","PH","FR","DE"];
console.log(countries);

// indexOf()
	// Returns the index number of the first matching element
	// found in an array. If no match has been found, the 
	// result will be -1. The search process will be done
	// from the proceeding to the last element.

	// Syntax:
	// arrayname.indexOf(searchValue);
	// arrayname.indexOf(searchValue, fromIndex);

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: "+firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: "+invalidCountry);

// lastIndexOf()
	// Returns the index number of the last matching item found
	// in the array.
	// The search process will be done from the last element proceeding
	// to first element.

	// Syntax:
	// arrayName.lastIndexOf(searchValue);
	// arrayName.lastIndexOf(searchValue, fromIndex);

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: "+lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 6); //5
//let lastIndexStart = countries.lastIndexOf("PH", 4); //1
console.log("Result of lastIndexOf method: "+lastIndexStart);

// slice()
	// Slices elements from an array and returns a new array

	// Syntax:
	// arrayname.slice(startingIndex);
	// arrayname.slice(startingIndex, endingIndex);

let sliceArray = countries.slice(2);
console.log("Result from slice method: ");
console.log(sliceArray);

let sliceArrayB = countries.slice(2,5);
console.log("Result from slice method: ");
console.log(countries);
console.log(sliceArrayB);
console.log("+++++++++++");

let sliceArrayC = countries.slice(-4, 6);
let sliceArrayD = countries.slice(-3, -2);
let sliceArrayE = countries.slice(3,-2);
console.log("Result from slice method: ");
console.log(sliceArrayC);
console.log(sliceArrayD);
console.log(sliceArrayE);

console.log("++++++++++++++");

// toString()
	// Returns an array as  string separated by commas

	// Syntax:
	// arrayName.toString();

let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);

// concat()
	// Combnies two array and returns the combined result

	// Syntax:
	// arraA.concat(elementA, elementB);

let tasksArrayA = ["Drink HTML","Eat Javascript"];
let tasksArrayB = ["Inhale CSS","Breathe Sass"];
let tasksArrayC = ["Get Git","Be Node"];

let tasksArrayD = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method:");
console.log(tasksArrayD);

let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

let allTaksA = tasksArrayB.concat(tasksArrayC, tasksArrayA);
console.log(allTaksA);

let combinedTask = tasksArrayA.concat("Smell Express", "Throw React");
console.log("Result from concat mthod: ");
console.log(combinedTask);


// join()
	// Returns an asrray as a string sperated by specified separator
	// string.

	// Syntax:
	// arrayName.join("seperatorString");

let users = ["John", "Jane","Joe","Robert"];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(' - '));


// Iteration Methods //

	// Are loops designed to perform repetitive taks on arrays
	// Useful for manipulating array data resulting in complex taks.

// foreach()
	// Similar to for loop that iterates on each array element

	// Syntax:
	// arrayName.foreach(function(individualElement){statement})

allTasks.forEach(function(tasks)
	{
		console.log(tasks);
	});

let students = ["Jeru", "Anne", "Glyn","Jake","Gabryl","Daniel"];

students.forEach(function(student)
{
	console.log(student);
});

console.log("++++++++++++++++++");

let filteredTasks = [];

allTasks.forEach(function(task)
{
	if (task.length > 10)
	{
		console.log(task);
		filteredTasks.push(task);
	}
});

console.log("Result of filtered forEach");
console.log(filteredTasks);

students.forEach(function(student)
{
	if (student.length <= 4)
	{
		console.log("To the left side: "+ student);
	}
	else
	{
		console.log("To the right side: "+ student);
	}
});


// map()
	// It iterates each element and returns new array with different
	// values depending on the result of the function's operation
	// This is useful for performing tasks where mutating/changing
	// the elements are required.
	// Unlike the forEach method, the map method requires the use of
	// a return statement in order to create another array with the
	// performed operation.

	// Syntax:
	// let/const resultArray = arrayName.map(function(individualElement));

let numbers = [1,2,3,4,5];

console.log("Before the map method: "+ numbers);

let numberMap = numbers.map(function(number)
{
	return number * number;
});

console.log("Result After Map method:");
console.log(numberMap);
console.log(numbers);

// every()
	// Checks if all element in an array meet the given condition
	// This is useful for validating data stored in arrays especially
	// when dealing with large amounts of data.
	// Returns the true value if all elements meet the condition and
	// false if otherwise.

	// Syntax:
	// let/const resultArray = arrayName.every(functio(element)
	// { return expression/condition})

let allValid = numbers.every(function(number)
{
	return (number < 3);
});

console.log("Result from every method:");
console.log(allValid);

// some()
	// Checks if atleast one element in an array meets the given
	// condition.
	// Returns a true if atleast one element meets the condition and
	// false if otherwise.

	// Syntax:
	// let/const resultArray = arrayName.some(function(element)
	// { return expression/condition});

let someValid = numbers.some(function(number)
{
	return (number < 2);
});

console.log("Result of some method:");
console.log(someValid);


// Combined result from every/some method //

if(someValid == true)
{
	console.log("Some numbers in the array are greater than 2");
}

// filter()
	// Returns new array that contains elements which meets the given
	// condition
	// Returns an empty array if no elements are found.
	// Useful for filtering array elements with a given condition and
	// shortens the syntax compared to using other array iteration
	// methods.

	// Syntax:
	// let/const resultArray = arrayName.filter(function(element)
	// { return expression/condition});

let filterValid = numbers.filter(function(number)
{
	return (number < 3);
});

console.log("Result of the filter method:");
console.log(filterValid);

let nothingFound = numbers.filter(function(number)
{
	return (number == 0);
});

console.log("Result of nothing filter:");
console.log(nothingFound);


let filteredNumbers = [];

numbers.forEach(function(number)
{
	console.log(number);

	if (number < 3)
	{
		filteredNumbers.push(number);
	}
});

console.log("Result from filter method again:");
console.log(filteredNumbers);
console.log("+++++++++++++++++++");


// includes()
	// Methods can be chained by using them one after another.
	// The result of the first method is used on the second method
	// until all chained methods have been solved.

	// Syntax:
	//

let products = ["Mouse","Keyboard","Laptop","Monitor"];

let filteredProducts = products.filter(function(product)
{
	return product.toLowerCase().includes("a");
});

console.log("Result from include method:");
console.log(filteredProducts);

// reduce()
	// Evaluates the statments from left to right and returns/ reduces
	// the array into a single value.

	// Syntax:
	// let/const resultArr = arrayName.reduce(function(accumulator, currentValue)
	// { return expression/ condition})

	// The accumulator parameter in the function stores the result
	// for every iteration of the loop.
	// The currentValue is the current/ next element in the array that
	// is evaluated in each iteration ofthe loop.

let iteration = 0;

let reducedArray = numbers.reduce(function(x,y)
{
	console.log("current iteration: "+ ++iteration);
	console.log("accumulator: "+ x);
	console.log("currentValue: "+ y);

	return x+y;
});

console.log("Result of reduce method: " + reducedArray);


// Reducing String Arrays

let list = ["Hello","Again","World"];

let reducedJoin = list.reduce(function(x,y)
{
	return x + ' ' + y;
})

console.log("Result of reduce method: "+ reducedJoin);

