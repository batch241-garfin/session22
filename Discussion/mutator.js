
// Array Methods //

	// Mutator Methods and Non-mutator Methods //

// Mutator Methods

	// Are functions that mutate or change an array after
	// theyre created.
	// These methods manipulate the original array.
	// Performing tasks such as adding and remobing elements.
	// push(), pop(),unshift(),shift(),splice(),sort(), & reverse()

let fruits = ["Apple","Orange","Kiwi","Dragon Fruit"];

console.log(fruits);

// push()
	// Adds in the end of the array and returns the array's length.

	// Syntax:
	// arrayName.push('elementA');

let fruitslength = fruits.push("Mango");
console.log(fruitslength);
console.log("Mutated array from push method:");
console.log(fruits);

// Push multiple elements to an array

fruits.push("Avocado", "Guava");
console.log("Mutated array from multiple push method");
console.log(fruits);

// pop()
	// Removes the last element in an array and returns
	// the removed element

	// Syntax:
	// arrayName.pop('elementA');

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

// unshift()
	// Adds at the start of an array

	// Syntax:
	// arrayname.unshift('elementA','elementB');
//console.log('%c Unshift Method:', 'background: #89CFF0')
fruits.unshift("Lime","Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

// shift()
	// Removes element at the start of an array and
	// returns the removed element
	
	// Syntax:
	// arrayname.shift('elementA');

//console.log('%c Shift Method:', 'background: #89CFF0');

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shit method:");
console.log(fruits);

// splice()
	// Simultaneously removes element from a specified number
	// and adds elements
	
	// Syntax:
	// arrayName.splice(stratingIndex, deleteCount, elementsToBeAdded);

fruits.splice(1,2, "Lime", "Cherry");
console.log("Mutated array from splice method:");
console.log(fruits);


// sort()
	// Rearramges the array elements in an Alphanumeric Order
	
	// Syntax:
	// arrayname.sort();

fruits.sort();
console.log("Mutated Array from sort method:");
console.log(fruits);

// reverse()
	// Reverses the order of array elements

	// Syntax:
	// arrayName.reverse();

fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);

