
/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

	function newUsers(newUser)
	{	
		let isExist = registeredUsers.some(function(user)
		{
			return (user === newUser);
		});

		if (isExist === true)
		{
			console.log("Registration failed. Username already exists.");
		}
		else
		{
			console.log("Thank you for registering");

			let successNewUser = registeredUsers.push(newUser);
		}
	}
	let newUser = prompt("Enter new user:");

	console.log(newUsers(newUser));
	//console.log(registeredUsers);
	console.log("");

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
	function addFriendList(addUser)
	{
		let isFriendExist = registeredUsers.some(function(user)
		{
			return (user === addUser);
		});

		if (isFriendExist === true)
		{
			console.log("You have added "+addUser+" as a friend!");
			let successNewFriend = friendsList.push(addUser);
		}
		else
		{
			console.log("User not found!");
		}
	}
	let toFriendlist1 = prompt("Enter a friend's name from registered list:");
    let toFriendlist2 = prompt("Enter a friend's name from registered list:");
    let toFriendlist3 = prompt("Enter a friend's name from registered list:");
    let toFriendlist4 = prompt("Enter a friend's name from registered list:");
    let toFriendlist5 = prompt("Enter a friend's name from registered list:");
    let toFriendlist6 = prompt("Enter a friend's name from registered list:");

	console.log(addFriendList(toFriendlist1));
    console.log(addFriendList(toFriendlist2));
    console.log(addFriendList(toFriendlist3));
    console.log(addFriendList(toFriendlist4));
    console.log(addFriendList(toFriendlist5));
    console.log(addFriendList(toFriendlist6));
    console.log("Your friendlist:");
	console.log(friendsList);
	console.log("");


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    function showFriends()
    {
    	if (friendsList.length === 0)
    	{
    		alert("You currently have 0 friends. Add one first.");
    	}
    	else
    	{
    		friendsList.forEach(function(friend)
			{
				console.log(friend);
			});
    	}
    }
    console.log("Your friendlist:");
    console.log(showFriends());
    console.log("");


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

    function amountAddFriend()
    {
    	if (friendsList.length === 0)
    	{
    		console.log("You currently have 0 friends. Add one first.");
    	}
    	else
    	{
    		alert("You currently have "+friendsList.length+" friends.")
    	}
    }

    console.log(amountAddFriend());
    console.log("");


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

    function deleteLastUser(user)
    {
        if (friendsList.length === 0)
        {
            console.log("You currently have 0 friends. Add one first.");
        }
        else
        {
            let answer = Number(prompt("You want to; 1. delete the last friend or 2. specific friend?"));

            if(answer === 2)
            {
                let targetFriend = prompt("Whose friend do you want to be removed from your friendlist?");
                let endIndex = Number(prompt("How many friends to you want to delete?"));

                let index = friendsList.indexOf(targetFriend);

                friendsList.splice(index, endIndex);
            }
            else if (answer === 1)
            {
                console.log("You have deleted a friend.");
                friendsList.pop();
            }
            else
            {
                console.log("Wrong Input:");
            }
        }
    }
    console.log(deleteLastUser());
    console.log("Your updated friendlist:");
    console.log(friendsList);
    console.log("");


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/








